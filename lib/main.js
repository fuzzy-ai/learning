// main.js -- main driver for learning microservice
// Copyright 2016 Fuzzy.ai
// All rights reserved

const LearningServer = require('./learning-server')

const server = new LearningServer(process.env)

server.start((err) => {
  if (err) {
    console.error(err)
    return process.exit(1)
  } else {
    return console.log('Server listening')
  }
})

const shutdown = function () {
  console.log('Shutting down...')
  return server.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)

process.on('uncaughtException', (err) => {
  console.error(err)
  if (err.stack) {
    console.dir(err.stack.split('\n'))
  }
  return process.exit(1)
})
