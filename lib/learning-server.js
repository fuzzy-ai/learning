// learning-server.js -- Learning server
// Copyright 2016 Fuzzy.ai
// All rights reserved

const fs = require('fs')
const path = require('path')

const _ = require('lodash')
const Microservice = require('@fuzzy-ai/microservice')
const debug = require('debug')('learning:server')

const FuzzyController = require('@fuzzy-ai/fuzzy-controller')
const {Trainer, Dataset} = require('@fuzzy-ai/fuzzy-learning')

const { HTTPError } = Microservice

const js = JSON.stringify

class LearningServer extends Microservice {
  getName () {
    return 'learning'
  }

  startDatabase (callback) {
    return callback(null)
  }

  stopDatabase (callback) {
    return callback(null)
  }

  setupRoutes (exp) {
    exp.get('/version', this.getVersion.bind(this))
    exp.get('/live', this.dontLog, this.live)
    exp.get('/ready', this.dontLog, this.ready)
    return exp.post('/optimize', this.optimize)
  }

  live (req, res, next) {
    return res.json({status: 'OK'})
  }

  ready (req, res, next) {
    return res.json({status: 'OK'})
  }

  optimize (req, res, next) {
    // Validate the inputs

    let dataset, err, fc
    if (!_.isObject(req.body)) {
      return next(new HTTPError('/optimize requires a body', 400))
    }

    if (!_.isObject(req.body.agent)) {
      return next(new HTTPError('/optimize requires an agent', 400))
    }

    if (!_.isObject(req.body.inputs)) {
      return next(new HTTPError('/optimize requires inputs', 400))
    }

    if (!_.isObject(req.body.expected)) {
      return next(new HTTPError('/optimize requires expected values', 400))
    }

    // Try to create a controller from the agent definition

    const { agent } = req.body

    try {
      fc = new FuzzyController(agent)
    } catch (error) {
      err = error
      return next(err)
    }

    // Dataset (admittedly, this is irritating)

    const colNames = _.concat(_.keys(agent.inputs), _.keys(agent.outputs))
    const outputColNames = _.keys(agent.outputs)

    debug(`colNames = ${colNames}`)
    debug(`outputColNames = ${outputColNames}`)

    const row = new Array(colNames.length)

    for (let i = 0, end = row.length - 1, asc = end >= 0; asc ? i <= end : i >= end; asc ? i++ : i--) {
      const name = colNames[i]
      if (Array.from(outputColNames).includes(name)) {
        row[i] = req.body.expected[name]
      } else {
        row[i] = req.body.inputs[name]
      }
    }

    debug(`row = ${js(row)}`)

    const rows = [row]

    debug(`rows = ${js(rows)}`)

    try {
      dataset = new Dataset(rows, colNames, outputColNames)
    } catch (error1) {
      err = error1
      return next(err)
    }

    debug(dataset)
    debug(`row[0] = ${js(dataset.row(0))}`)

    // create the trainer

    const trainer = Trainer.load('gradient-descent')

    // train the data (this might take some CPU time)

    try {
      trainer.trainToDataset(fc, dataset)
    } catch (error2) {
      err = error2
      return next(err)
    }

    // return the results

    return res.json(fc.toJSON())
  }

  getVersion (req, res, next) {
    return this.packageVersion((err, version) => {
      if (err) {
        return next(err)
      } else {
        return res.json({name: this.getName(), version})
      }
    })
  }

  packageVersion (callback) {
    if (this.version) {
      return callback(null, this.version)
    } else {
      const pkg = path.join(__dirname, '..', 'package.json')
      return fs.readFile(pkg, 'utf8', (err, data) => {
        if (err) {
          return callback(err)
        } else {
          let doc
          try {
            doc = JSON.parse(data)
          } catch (error) {
            return callback(error)
          }
          this.version = doc.version
          return callback(null, this.version)
        }
      })
    }
  }
}

module.exports = LearningServer
