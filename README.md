learning
========

This microservice does the learning and optimization tasks for the fuzzy.ai
platform.

Configuration
-------------

This microservice uses the
[Microservice](https://github.com/fuzzy-ai/microservice) library, so it takes
all the same configuration options via environment variables.

It doesn't make a database connection so `DRIVER` and `PARAMS` are unnecessary.

Endpoints
---------

* `GET /version`

Returns a JSON object with a `name` property that should always be "learning"
and a `version` property that should be a [semver](http://semver.org/)
compatible version.

* `POST /optimize`

Optimizes an agent. Accepts a JSON object with these properties:

  * `agent`: the agent to optimize, including `inputs`, `outputs`, and `rules`
    or `parsed_rules`.
  * `inputs`: the input values to pass to the agent.
  * `expected`: the expected outputs. Optimizing the agent entails changing the
    properties of the agent to make it return values closer to these values when
    it receives the inputs.

Returns a JSON object with the new agent value (`inputs`, `outputs`, and
`rules`).

* `GET /live`

Returns `{status: "OK"}`. Good for checking if the server is live.

* `GET /ready`

Returns `{status: "OK"}`. Good for checking if the server is ready.
