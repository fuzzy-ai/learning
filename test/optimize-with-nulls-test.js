// server-test.js -- Launch server and test HTTP interface
// Copyright 2016 Fuzzy.ai
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const web = require('@fuzzy-ai/web')

const env = require('./env')
const apiBatch = require('./apibatch')

const base = {
  inputs: {
    input1: {
      veryLow: [0, 25],
      low: [0, 25, 50],
      medium: [25, 50, 75],
      high: [50, 75, 100],
      veryHigh: [75, 100]
    },
    input2: {
      veryLow: [0, 25],
      low: [0, 25, 50],
      medium: [25, 50, 75],
      high: [50, 75, 100],
      veryHigh: [75, 100]
    }
  },
  outputs: {
    output1: {
      veryLow: [0, 25],
      low: [0, 25, 50],
      medium: [25, 50, 75],
      high: [50, 75, 100],
      veryHigh: [75, 100]
    }
  },
  rules: [
    'input1 INCREASES output1 WITH 0.5',
    'input2 DECREASES output1 WITH 0.5'
  ]
}

vows.describe('POST /optimize with nulls')
  .addBatch(apiBatch({
    'and we post a model to /optimize': {
      topic () {
        const hostname = env.HOSTNAME || 'localhost'
        const port = env.PORT || '80'
        const url = `http://${hostname}:${port}/optimize`
        const headers = {
          'Content-Type': 'application/json; charset=utf-8'
        }
        const payload = JSON.stringify({
          agent: base,
          inputs: {
            input1: 50,
            input2: null
          },
          expected: {
            output1: 45
          }
        })
        web.post(url, headers, payload, this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        return assert.isString(body)
      },
      'it looks correct' (err, response, body) {
        let doc
        assert.ifError(err)
        assert.isObject(response)
        assert.isString(body)
        try {
          doc = JSON.parse(body)
        } catch (error) {
          assert.ifError(error)
        }
        return assert.isObject(doc)
      }
    }
  })).export(module)
