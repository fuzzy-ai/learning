// env.js -- Environment for unit tests
// Copyright 2016 Fuzzy.ai
// All rights reserved

module.exports = {
  HOSTNAME: 'localhost',
  PORT: '2342',
  LOG_FILE: '/dev/null',
  SILENT: 'true'
}
