// apibatch.js -- Launch server and test HTTP interface
// Copyright 2016 Fuzzy.ai
// All rights reserved

const {fork} = require('child_process')
const path = require('path')

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const env = require('./env')

const apiBatch = function (rest) {
  const batch = {
    'When we launch a server': {
      topic () {
        const { callback } = this
        // pass DEBUG through for debug module
        if (process.env.DEBUG != null) {
          env.DEBUG = process.env.DEBUG
        }
        const full = path.join(__dirname, '..', 'lib', 'main.js')
        const child = fork(full, [], {env, silent: true})
        child.once('error', err => callback(err))
        child.stderr.on('data', (data) => {
          const str = data.toString('utf8')
          return process.stderr.write(`SERVER ERROR: ${str}\n`)
        })
        child.stdout.on('data', (data) => {
          const str = data.toString('utf8')
          if (str.match('Server listening')) {
            return callback(null, child)
          } else if (env.SILENT !== 'true') {
            return process.stdout.write(`SERVER: ${str}\n`)
          }
        })
        return undefined
      },
      'it works' (err, child) {
        assert.ifError(err)
        return assert.isObject(child)
      },
      'teardown' (child) {
        // Send SIGKILL after 10 seconds if it doesn't shut down from
        // SIGTERM
        const sendKill = () => child.kill('SIGKILL')
        const timeout = setTimeout(sendKill, 10000)
        child.on('close', (code, signal) => {
          clearTimeout(timeout)
          return this.callback(null)
        })
        child.kill('SIGTERM')
        return undefined
      }
    }
  }

  _.assign(batch['When we launch a server'], rest)

  return batch
}

module.exports = apiBatch
