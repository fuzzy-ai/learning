// live-test.js
// Copyright 2017 Fuzzy.ai
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const web = require('@fuzzy-ai/web')

const env = require('./env')
const apiBatch = require('./apibatch')

vows.describe('GET /live')
  .addBatch(apiBatch({
    'and we get /live': {
      topic () {
        const hostname = env.HOSTNAME || 'localhost'
        const port = env.PORT || '80'
        const url = `http://${hostname}:${port}/live`
        web.get(url, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else {
            let doc
            try {
              doc = JSON.parse(body)
            } catch (error) {
              err = error
              return this.callback(err)
            }
            return this.callback(null, doc)
          }
        })
        return undefined
      },
      'it works' (err, doc) {
        assert.ifError(err)
        assert.isObject(doc)
        return assert.equal(doc.status, 'OK')
      }
    }
  })).export(module)
