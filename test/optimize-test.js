// server-test.js -- Launch server and test HTTP interface
// Copyright 2016 Fuzzy.ai
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const debug = require('debug')('learning:optimize-test')
const web = require('@fuzzy-ai/web')

const env = require('./env')
const apiBatch = require('./apibatch')

const js = JSON.stringify

const MAX_DISTANCE = 500

const { PI } = Math

const base = {
  inputs: {
    distance: {
      veryLow: [0, MAX_DISTANCE / 4],
      low: [0, MAX_DISTANCE / 4, MAX_DISTANCE / 2],
      medium: [MAX_DISTANCE / 4, MAX_DISTANCE / 2, (3 * MAX_DISTANCE) / 4],
      high: [MAX_DISTANCE / 2, (3 * MAX_DISTANCE) / 4, MAX_DISTANCE],
      veryHigh: [(3 * MAX_DISTANCE) / 4, MAX_DISTANCE]
    }
  },
  outputs: {
    angle: {
      veryLow: [0, PI / 16],
      low: [0, PI / 16, PI / 8],
      medium: [PI / 16, PI / 8, (3 * PI) / 16],
      high: [PI / 8, (3 * PI) / 16, PI / 4],
      veryHigh: [(3 * PI) / 16, PI / 4]
    }
  },
  rules: [
    'distance INCREASES angle WITH 0.5'
  ]
}

vows.describe('POST /optimize')
  .addBatch(apiBatch({
    'and we post a model to /optimize': {
      topic () {
        const hostname = env.HOSTNAME || 'localhost'
        const port = env.PORT || '80'
        const url = `http://${hostname}:${port}/optimize`
        const headers = {
          'Content-Type': 'application/json; charset=utf-8'
        }
        const payload = JSON.stringify({
          agent: base,
          inputs: {
            distance: (3 * MAX_DISTANCE) / 8
          },
          expected: {
            angle: 0.5 * Math.asin(3.0 / 8.0)
          }
        })
        web.post(url, headers, payload, this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        debug(js(err))
        assert.ifError(err)
        assert.isObject(response)
        return assert.isString(body)
      },
      'it looks correct' (err, response, body) {
        let doc
        assert.ifError(err)
        assert.isObject(response)
        assert.isString(body)
        try {
          doc = JSON.parse(body)
        } catch (error) {
          assert.ifError(error)
        }
        return assert.isObject(doc)
      }
    }
  })).export(module)
