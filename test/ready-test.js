// ready-test.js
// Copyright 2017 Fuzzy.ai
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const web = require('@fuzzy-ai/web')

const env = require('./env')
const apiBatch = require('./apibatch')

vows.describe('GET /ready')
  .addBatch(apiBatch({
    'and we get /ready': {
      topic () {
        const hostname = env.HOSTNAME || 'localhost'
        const port = env.PORT || '80'
        const url = `http://${hostname}:${port}/ready`
        web.get(url, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else {
            let doc
            try {
              doc = JSON.parse(body)
            } catch (error) {
              err = error
              return this.callback(err)
            }
            return this.callback(null, doc)
          }
        })
        return undefined
      },
      'it works' (err, doc) {
        assert.ifError(err)
        assert.isObject(doc)
        return assert.equal(doc.status, 'OK')
      }
    }
  })).export(module)
