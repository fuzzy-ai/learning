// server-test.js -- Launch server and test HTTP interface
// Copyright 2016 Fuzzy.ai
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const web = require('@fuzzy-ai/web')

const env = require('./env')
const apiBatch = require('./apibatch')

vows.describe('GET /version')
  .addBatch(apiBatch({
    'and we get the /version': {
      topic () {
        const hostname = env.HOSTNAME || 'localhost'
        const port = env.PORT || '80'
        const url = `http://${hostname}:${port}/version`
        web.get(url, (err, response, body) => {
          if (err) {
            return this.callback(err)
          } else {
            let doc
            try {
              doc = JSON.parse(body)
            } catch (error) {
              err = error
              return this.callback(err)
            }
            return this.callback(null, doc)
          }
        })
        return undefined
      },
      'it works' (err, doc) {
        assert.ifError(err)
        return assert.isObject(doc)
      },
      'it looks correct' (err, doc) {
        assert.ifError(err)
        assert.isObject(doc)
        assert.isString(doc.name)
        assert.equal(doc.name, 'learning')
        return assert.isString(doc.version)
      }
    }
  })).export(module)
