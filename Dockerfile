FROM node:9-alpine

RUN apk add --no-cache curl

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ARG NPM_TOKEN
ENV NPM_TOKEN $NPM_TOKEN
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
RUN echo "//registry.npmjs.org/:_authToken=\${NPM_TOKEN}" > /root/.npmrc
COPY package.json /usr/src/app/
RUN apk add --no-cache make gcc g++ python && \
  npm install && \
  npm cache clean --force && \
  apk del make gcc g++ python
RUN rm -f /root/.npmrc
COPY . /usr/src/app

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/usr/src/app/bin/dumb-init", "--"]
CMD ["/usr/local/bin/node", "lib/main.js"]
